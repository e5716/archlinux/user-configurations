#!/bin/fish

# This script is integrated with a systemd timer so it can download on a ¿daily? basis the resources specified in the config file

# Format in the config file: "audio/video::<URL_OF_RESOURCE>::<DESTINATION_FOLDER>"

# Protip: for extracting the IDs of files already downloaded inside a folder and creating the file that would help to skip existing media, run the following inside that directory:
# for I in (ls); set ID (echo $I | rev | cut -d ']' -f 2 | cut -d '[' -f 1 | rev); echo youtube $ID >> .skip_files; end
# After that, just copy the ".skip_files" to the folder where the new media will be downloaded

set CONFIG_FILE (string join '' $MY_SCRIPTS_CONFIG_ROOT '/recurring-yt-downloader.conf')

function readConfigFile
    set LIST_OF_RESOURCES (cat $CONFIG_FILE)

    for LINE in $LIST_OF_RESOURCES
        set resource (string split '::' $LINE)

        # This must be an array, so don't try to concatenate anything because the internal $argv of the functions won't recognize them as arguments to be passed to the yt-dlp executable. Anything that is separated by an space must be added independently to the array, for example "--download-archive" and ".existing_files" must be on their own slots even when they're part of the same thing under yt-dlp
        # Use the flag "--simulate" to execute a dry-run under yt-dlp
        # With "--download-archive" we can skip those media already downloaded. It looks like yt-dlp will update this file automatically so the next run those media files won't be downloaded again
        set args_for_ytdlp '--paths' $resource[3] '--download-archive' (string join '' $resource[3] '/.skip_files') $resource[2]

        # Functions audio-dl and video-dl are coming from my personal Fish configuration files, and internally use "yt-dlp". See files under "config/fish/functions"
        switch $resource[1]
            case 'audio'
                audio-dl $args_for_ytdlp
            case 'video'
                video-dl $args_for_ytdlp
            case '*'
                echo Unknown type of resource
        end
    end
end

if test -f $CONFIG_FILE
    readConfigFile
else
    echo 'There was an error reading config file'
    exit 1
end

exit
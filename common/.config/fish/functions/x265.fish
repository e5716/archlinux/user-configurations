function x265
	ffmpeg -i "$argv" -c:v libx265 -preset medium -acodec libopus -b:a 320k "$argv.mkv"
end
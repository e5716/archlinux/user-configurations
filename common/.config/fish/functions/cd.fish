function cd
	if builtin cd $argv
		lsd -a1
	end
end
# Each time I change directory, I like to list the files
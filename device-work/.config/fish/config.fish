# Remove start message
set fish_greeting

# Only executed when there is a new login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR -eq 1
        exec startx -- -keeptty
    end
end

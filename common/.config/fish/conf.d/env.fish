# Add routes to PATH
fish_add_path -a '/usr/bin' '/opt/android-sdk/platform-tools' '/opt/android-sdk/tools/bin' '/opt/android-sdk/emulator' '~/.local/bin' 

# Avoiding Bash history file (yes, i said Bash, not Fish)
set --export HISTFILE /dev/null

# To being able to connect to "systemd start --user ssh-agent", based on https://wiki.archlinux.org/index.php/SSH_keys#Start_ssh-agent_with_systemd_user
set --export SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/ssh-agent.socket"

# Firefox for Wayland + HVA
set --export MOZ_WEBRENDER 1
set --export MOZ_ENABLE_WAYLAND 1
set --export MOZ_DISABLE_RDD_SANDBOX 1 # For Hardware Video Acceleration
set --export MOZ_DBUS_REMOTE 1 # To enable other apps to run Firefox (links opening)

# Trash for VSCodium
set --export ELECTRON_TRASH trash-cli

# Important for some CLI tools
set --export VISUAL nano
set --export EDITOR nano
set --export PAGER less

# My personal scripts
set --export MY_SCRIPTS_ROOT $HOME/.local/bin
set --export MY_SCRIPTS_CONFIG_ROOT $HOME/.config/.meine-Skripts
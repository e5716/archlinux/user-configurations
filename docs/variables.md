# Variables

Most of my custom scripts use the `variables.yml` file, located outside of this repository as it might contain sensitive data, for example, to unlocking disks.

The location of this file is defined at `$MY_SCRIPTS/lib/yaml-variables.bash` which is a file loaded by several scripts to read variables defined inside that YAML file.

# Requirements

- [yq](https://archlinux.org/packages/extra/any/yq/)

# Best practices

- You must store this file outside of any tracked repository. At the same time, you must backup this file periodically.

TODO: put here a template/example of my current variables file and explain relevant parts
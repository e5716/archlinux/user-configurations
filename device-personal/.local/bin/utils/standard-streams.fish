function echoStdout
    echo $argv
end
function echoStderr
    echo $argv >&2
end
function haltExecution
    echoStderr "$argv[1]"
    exit 1
end
function gpg-encrypt-files
    if set -q argv[1]
        for SOURCEFILE in (/usr/bin/ls .)
            echo "$argv[1]" | gpg --batch --passphrase-fd 0 --symmetric --cipher-algo AES256 "$SOURCEFILE"
        end
    else
        echo "No password provided"
    end
end
# GPG symmetric encryption and decryption
# All the files in the $PWD folder will be encrypted with the supplied password. The first arg is the password
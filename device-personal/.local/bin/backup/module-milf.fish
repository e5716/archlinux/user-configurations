
# This backup process generates a small file with very important files that could help to restore almost everything else. This file should be dispersed across different devices after its generation, but this is not a responsibility of this script, so make sure to provide outputs folders that you sync regularly with other devices/cloud storage
# MILF = Most Important Local Files

function _generateCompressedFile
    set FASTMAIL_FILE_MAX_SIZE "240m" # Fastmail allows files up to 250MB: https://www.fastmail.help/hc/en-us/articles/1500000280121-About-Fastmail-file-storage#quota

    set -g milfTmpDir (mktemp -d) # This is more secure when the name is fully random instead of providing a filename template to mktemp
    yamlGetValue ".milf.output_file.name" "$backupConfigFile"
    set milfOutputFile "$milfTmpDir/$YAML_VALUE"

    yamlGetValue ".milf.sources[]" "$backupConfigFile"
    for SOURCE in $YAML_VALUE
        set -a sources "$SOURCE"
    end

    yamlGetValue ".milf.output_file.encryption_pwd" "$backupConfigFile"
    7zCreateArchive "0" "$YAML_VALUE" "$FASTMAIL_FILE_MAX_SIZE" "$milfOutputFile" $sources; or haltExecution # I need encryption to store this file in Fastmail because it looks like Fastmail distributes their cloud files over HTTP (WebDAV)
end
function _distributeFile
    yamlGetValue ".milf.destinations[]" "$backupConfigFile"
    for DESTINATION in $YAML_VALUE
        rsyncCopy $milfTmpDir/* $DESTINATION; or haltExecution
    end
end
function _milfCleanup
    rmFileOrDir "$milfTmpDir"
    set --erase milfTmpDir
    # ToDo: delete the distributed file (a bit hard to find because is deployed to more than one directory)
end
function runMilf
    _generateCompressedFile
    _distributeFile
    _milfCleanup
end
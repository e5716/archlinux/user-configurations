function 7zCreateArchive
    set compressionLevel "$argv[1]"
    set compressionPwd "$argv[2]"
    set compressionChunkSize "$argv[3]"
    set outputFile "$argv[4]"
    set sources $argv[5..-1]

    7za a -t7z -mx"$compressionLevel" -p"$compressionPwd" -mhe=on -v"$compressionChunkSize" "$outputFile" $sources # 7-Zip might be better: already includes encryption and can split the file in smaller chunks, but cannot store some extended attributes

    return $0
end
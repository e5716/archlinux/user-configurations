# Replacement of popular CLI utilities
alias cat='bat'
alias find='fd'
alias grep='rg' # For finding text inside files (text, PDFs and much more), search based on content. Thing you already use in VSCodium but more limited. It also includes a utility called "rga-fzf"
alias htop='btop'
alias ls='lsd'
alias rm='trash'

# Improve commands
alias uninstall='sudo pacman -Rscn'
alias x='exit'
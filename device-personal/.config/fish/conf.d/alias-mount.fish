# Offline storage
alias mount-disk-archbak='fish $MY_SCRIPTS_ROOT/offline-disks.fish -o mount -d archbak'
alias mount-disk-mirror999='fish $MY_SCRIPTS_ROOT/offline-disks.fish -o mount -d mirror999'

alias unmount-disk-archbak='fish $MY_SCRIPTS_ROOT/offline-disks.fish -o unmount -d archbak'
alias unmount-disk-mirror999='fish $MY_SCRIPTS_ROOT/offline-disks.fish -o unmount -d mirror999'

# Cloud storage
alias mount-cloud-smallfiles='rclone mount --no-modtime --no-seek --read-only Cloud-Fastmail:/ $HOME/.fuse-mountpoints/rclone-fastmail/'

# Others
alias mount-5g-er='gocryptfs $HOME/Others/5Gr $HOME/.fuse-mountpoints/gocryptfs-5gr'
alias unmount-5g-er='umount $HOME/.fuse-mountpoints/gocryptfs-5gr'
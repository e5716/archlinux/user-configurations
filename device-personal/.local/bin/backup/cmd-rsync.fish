function rsyncCopy
    cp $argv[1] $argv[2] # Using aliased "cp" that should point to rsync. Do not enclose filepaths in quotes here, or globbing expansion coming from upstream won't work
    return $0
end
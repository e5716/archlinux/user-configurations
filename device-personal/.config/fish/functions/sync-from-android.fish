function sync-from-android
    fish $MY_SCRIPTS_ROOT/android-sync.fish --operation pull --serial $argv[1] --name $argv[2]
end
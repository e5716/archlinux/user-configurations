function _standarizeFilenames
    # I need this solution because adbsync has bugs when creating the diff list with filenames with strange characters, like emojis or those weird Youtube titles (yt-dlp creates files with those names if the video has weird characters)
    yamlGetValue ".android_sync.detox.ignore_file" "$backupConfigFile"
    set detoxIgnore "$YAML_VALUE"
    yamlGetValue ".android_sync.detox.sources[]" "$backupConfigFile"
    for SOURCE in $YAML_VALUE
        set -a detoxDirs "$SOURCE"
    end
    
    detoxFilenames "$detoxIgnore" $detoxDirs
end
function _evalAndExecAndroidSync
    for smartphone in $argv
        if string match -i -r -q '^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\:(\d{1,5})$' "$smartphone"
            set -g --append androidDeviceIpAddresses (string split -f 1 '-' $smartphone)
        else
            haltExecution "The argument '$smartphone' does not follow the format '--smartphone <IP address>:<port>'"
        end
    end

    # Note: adb doesn't report an exit status when the connection/disconnection fails so I cannot handle errors here
    yamlGetValue ".android_sync.selected_device" "$backupConfigFile"
    for I in (seq 1 (count $androidDeviceIpAddresses))
        _syncSteps "$androidDeviceIpAddresses[$I]" "$YAML_VALUE"
    end
end
function _androidSync
    argparse 's/smartphone=+' -- $argv

    if test -n "$_flag_s"
        _evalAndExecAndroidSync $_flag_s
    end
end
function _syncSteps
    adb connect "$argv[1]"
    sync-from-android "$argv[1]" "$argv[2]"
    sync-to-android "$argv[1]" "$argv[2]"
    adb disconnect "$argv[1]"
end
function runAndroidSync
    _standarizeFilenames
    _androidSync $argv
end
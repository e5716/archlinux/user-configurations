function audio-dl
	# No more re-encoding: https://superuser.com/a/1337237
	yt-dlp -f bestaudio --extract-audio --no-playlist $argv
end
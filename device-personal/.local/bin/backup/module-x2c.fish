function _processEachItem
    yamlGetValue ".x2c.clouds[$argv[1]].source" "$backupConfigFile"
    set origin "$YAML_VALUE"
    yamlGetValue ".x2c.clouds[$argv[1]].destination" "$backupConfigFile"
    set destination "$YAML_VALUE"

    rcloneSync "$argv[2]" "$origin" "$destination"
end
function runExtrasToCloud
    yamlGetValue ".common.rclone_config_pwd" "$backupConfigFile"
    set rclonePwd "$YAML_VALUE"

    yamlGetValue ".x2c.clouds | length" "$backupConfigFile"
    for I in (seq 0 (count $YAML_VALUE))
        _processEachItem $I "$rclonePwd"
    end
end
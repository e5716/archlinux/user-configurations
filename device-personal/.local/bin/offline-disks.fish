#!/bin/fish

# This solution tries to avoid using standard LUKS way of mounting disks because it requires root privileges

# It is a good idea run periodically "tune2fs -l /dev/ext4_disk" and check the info returned (mainly row "Filesystem state: clean with errors"), look for filesystem errors and try to fix them with "fsck -f /dev/ext4_disk" (runs a check) and "fsck /dev/ext4_disk". IMPORTANT: the "ext4_disk" MUST BE UNMOUNTED BEFORE DOING ANYTHING

source $MY_SCRIPTS_ROOT/utils/standard-streams.fish
source $MY_SCRIPTS_ROOT/utils/yaml.fish

set CONST_VALID_OPERATIONS "mount" "unmount"
set CONST_PATH_DEV_DISK_BY_UUID "/dev/disk/by-uuid"
set CONST_PATH_DEV_LUKS_UUID "/dev/mapper/luks-" # Currently, udisks maps the unlocked file to this path and appends the UUID of the unlocked device for mounting

function init
    if test -z "$argv[1]"; or test -z "$argv[2]"
        haltExecution "Missing flags: -o, -d"
    end

    if not contains "$argv[1]" $CONST_VALID_OPERATIONS
        haltExecution "Not a valid operation: '$argv[1]'"
    end

    set -g operation "$argv[1]"
    set -g name "$argv[2]"
end

function loadConfiguration
    yamlGetValue ".$name" "$configurationFile"
    if test -z "$YAML_VALUE"; or test "$YAML_VALUE" = "null"
        haltExecution "Device not found in configuration file"
    else
        yamlGetValue ".$name.disk_uuid" "$configurationFile"
        set -g deviceUuid $YAML_VALUE
        yamlGetValue ".$name.luks_keyfile" "$configurationFile"
        set -g deviceKeyfile $YAML_VALUE
    end
end

function runOperation
    switch $operation
        case 'mount'
            udisksctl unlock --block-device "$CONST_PATH_DEV_DISK_BY_UUID/$deviceUuid" --key-file "$deviceKeyfile" --no-user-interaction; or haltExecution "udisks failed"
            udisksctl mount --block-device "$CONST_PATH_DEV_LUKS_UUID$deviceUuid" --no-user-interaction; or haltExecution "udisks failed"
        case 'unmount'
            sync; or haltExecution "sync failed"
            udisksctl unmount --block-device "$CONST_PATH_DEV_LUKS_UUID$deviceUuid" --no-user-interaction; or haltExecution "udisks failed"
            udisksctl lock --block-device "$CONST_PATH_DEV_DISK_BY_UUID/$deviceUuid" --no-user-interaction; or haltExecution "udisks failed"
            udisksctl power-off --block-device "$CONST_PATH_DEV_DISK_BY_UUID/$deviceUuid" --no-user-interaction; or haltExecution "udisks failed"
    end
end

argparse 'o/operation=' 'd/device=' -- $argv; or haltExecution "Failed to parse arguments"
set -g configurationFile "$MY_SCRIPTS_CONFIG_ROOT/offline-disks.yaml"
init "$_flag_o" "$_flag_d"
loadConfiguration
runOperation

exit 0
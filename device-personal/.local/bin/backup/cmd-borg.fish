# Te voy a poner unos buenos borgazos ;v

function borgCheck
    BORG_REPO="$argv[1]" BORG_PASSPHRASE="$argv[2]" borg --progress check --verify-data
    return $0
end
function borgCreateArchive
    BORG_REPO="$argv[1]" BORG_PASSPHRASE="$argv[2]" borg --progress create --stats --compression "$argv[3]" --exclude-from "$argv[4]" ::"$argv[5]" "$argv[6]"
    return $0
end
function borgPrune
    BORG_REPO="$argv[1]" BORG_PASSPHRASE="$argv[2]" borg --progress prune --stats --list --keep-last "$argv[3]"
    return $0
end
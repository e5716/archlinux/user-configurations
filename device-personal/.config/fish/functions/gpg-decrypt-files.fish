function gpg-decrypt-files
    if set -q argv[1]
        for SOURCEFILE in (/usr/bin/ls .)
            set --local OUTPUTFILE (echo "$SOURCEFILE" | string split --right --fields 1 ".gpg")
            echo "$argv[1]" | gpg --batch --passphrase-fd 0 --output "$OUTPUTFILE" --decrypt "$SOURCEFILE"
        end
    else
        echo "No password provided"
    end
end
function superuserExec
    sudo -k # Invalidates the session cache, it enforces to request sudo password again
    echo "$suPwd" | sudo -E -S -p "" -- fish -c "$argv[1]" # With "--" we indicate all the following params belong the command to execute, is a separator of arguments
    return $0
end
function superuserStorePassword
    # Storing the password to be used later, only for those instructions that really need superuser privileges. With that we avoid to run this whole script with elevated permissions and also the user does not need to enter the sudo password again hours later, due to tasks that take a long time to be finished, like borg checks
    set -g suPwd (read -s -P "[Superuser] password: ")
    superuserExec ":"; or haltExecution "Superuser permissions check failed...¿wrong password?" # ":" is the no-operation command (it does nothing). With this line I'm comparing the provided password is correct
end
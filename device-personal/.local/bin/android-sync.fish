#!/bin/fish

# This solution works with symlinks; the source folders defined in configuration file host all the files you want to copy to your device in shape of symlinks

source $MY_SCRIPTS_ROOT/utils/standard-streams.fish
source $MY_SCRIPTS_ROOT/utils/android-adb.fish
source $MY_SCRIPTS_ROOT/utils/yaml.fish
source $MY_SCRIPTS_ROOT/utils/timestamp.fish

set CONST_VALID_OPERATIONS "push" "pull"

function init
    if test -z "$argv[1]"; or test -z "$argv[2]"; or test -z "$argv[3]"
        haltExecution "Missing flags: -o, -s, -n"
    end

    if not contains "$argv[1]" $CONST_VALID_OPERATIONS
        haltExecution "Not a valid operation: '$argv[1]'"
    end

    set -g deviceOperation "$argv[1]"
    set -g deviceSerial "$argv[2]"
    set -g deviceName "$argv[3]"
end

function loadDeviceGeneralConfiguration
    set -g auxSelector (string join "" "." $deviceOperation "[]")

    yamlGetValue "$auxSelector | select(.name == \"$deviceName\") | .directories | length" "$configurationFile" # yq does not accept single quotes
    if test -z $YAML_VALUE
        haltExecution "Device not found in configuration file"
    else
        set -g upperLimitOfDirectoriesList (math $YAML_VALUE - 1)
    end
end

function preSteps
    if test "$deviceOperation" = "pull"
        set -g auxDestinationSubfolder (string join "" "android_backup_" (timestamp))
    end
end

function runSync
    for I in (seq 0 $upperLimitOfDirectoriesList)
        setSourceFolder "$I"
        setDestinationFolder "$I"
        syncAndroid
    end
end

function setSourceFolder
    set --erase sourceFolder
    yamlGetValue "$auxSelector | select(.name == \"$deviceName\") | .directories[$argv[1]].source" "$configurationFile"
    set -g sourceFolder $YAML_VALUE
end

function setDestinationFolder
    set --erase destinationFolder
    yamlGetValue "$auxSelector | select(.name == \"$deviceName\") | .directories[$argv[1]].destination" "$configurationFile"
    set auxDestinationFolder $YAML_VALUE

    if test "$deviceOperation" = "pull"
        set -g destinationFolder (string join "" $auxDestinationFolder "/" "$auxDestinationSubfolder")
        if not test -d "$destinationFolder"
            mkdir "$destinationFolder"
        end
    else
        set -g destinationFolder "$auxDestinationFolder"
    end
end

function syncAndroid
    # Using https://github.com/jb2170/better-adb-sync, it does not support more than one source folder and fails if the folder on the device does not exist
    # DON't handle if adbsync fails, remember it's executed once per origin folder. If it does not exist, it will stop the executing of all the script so the remaining folders won't be synced. I can tolerate that
    adbsync --copy-links --del --show-progress "$deviceOperation" "$sourceFolder" "$destinationFolder"
end

argparse 'o/operation=' 's/serial=' 'n/name=' -- $argv; or haltExecution "Failed to parse arguments"
set -g configurationFile "$MY_SCRIPTS_CONFIG_ROOT/android-sync.yaml"
init "$_flag_o" "$_flag_s" "$_flag_n"
checkDeviceConnected "$deviceSerial"
loadDeviceGeneralConfiguration
preSteps
runSync

exit 0
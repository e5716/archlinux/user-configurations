source $MY_SCRIPTS_ROOT/utils/standard-streams.fish

function checkDeviceConnected
    adb -s "$argv[1]" get-state > /dev/null 2>&1
    if test $status -ne 0
        haltExecution "Smartphone '$argv[1]' not connected"
    end
end
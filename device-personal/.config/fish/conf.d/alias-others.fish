# Backups
alias personal-backup='time fish $MY_SCRIPTS_ROOT/personal-backup.fish'

# Others
alias maintenance='fish $MY_SCRIPTS_ROOT/system-maintenance.fish'
alias reset-mime-associations='fish $MY_SCRIPTS_ROOT/reset-file-associations.fish'
alias smartphone-virtual='QT_QPA_PLATFORM=xcb emulator @Virtual -camera-back emulated -camera-front emulated -no-snapshot -no-cache -no-audio' # -phone-number 4490001111 -camera-front webcam0
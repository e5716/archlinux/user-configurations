function detoxFilenames
    # Detox can't follow symlinks
    fd -L -H --ignore-file "$MY_SCRIPTS_CONFIG_ROOT/$argv[1]" . $argv[2..-1] -X detox -r -s utf_8 {} # It looks like the "utf_8" sequence is the best that fits my needs
    return $0
end
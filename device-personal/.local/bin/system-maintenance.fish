# This script is for recurring system maintenance. Try to keep it simple

# POLICY: try to avoid installing external packages with "sudo npm / sudo pip", instead search for its equivalent in the AUR. For user packages (without using sudo), it's fine if you use "npm / pip" or any other package manager

function createSnapshot
	# When you need the snapshot, simply merge the snapshot using "lvconvert --merge" or mount the snapshot somewhere else and restore the files with Rsync. https://wiki.archlinux.org/title/LVM#Snapshots https://wiki.archlinux.org/title/Create_root_filesystem_snapshots_with_LVM
	# Consideration: the current empty space is 30GB but the real root partition size is 50GB. This is because I don't want to have unused and wasted empty space "a lo pendejo", so I will only leave a fraction of the real size. Also, today my root partition is around 17GB worth of used space
	# Note: dirty values, too lazy to implement yq parsing in Fish
	sudo lvremove -f /dev/GMOW/root-snapshot; or exit 1
	sudo lvcreate --size 30G --snapshot --name root-snapshot /dev/GMOW/root; or exit 1 # 30G is the current empty size I currently have under my LVM group
end

function update
	# Reflector is no longer needed in this script. Make sure the systemd timer of Reflector is enabled; it'll run once a week
	paru -Syu

	# Firmware updates
	sudo fwupdmgr refresh
	sudo fwupdmgr get-updates
	sudo fwupdmgr update

end

function cleanup
	# paccache manual cleaning is no longer needed; the systemd timer of paccache should be enabled by now

	sudo pacman -Rscn (pacman -Qtdq)

	# Run it with sudo as some files might be owned by root
	sudo /usr/bin/rm -rf $HOME/.cache
	sudo /usr/bin/rm -rf $HOME/.thumbnails 
	trash-empty
	
	# Delete Fish history, as I haven't found a solution to disable it in the configuration
	builtin history clear # This script MUST be executed by fish, so the builtin will be recognized
end

createSnapshot
update
cleanup
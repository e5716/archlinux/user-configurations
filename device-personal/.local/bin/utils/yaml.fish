# It uses https://archlinux.org/packages/extra/x86_64/go-yq, be careful about which "yq" package you have installed

# Note: if you want to store the result of $VAR_UTILS_YAML_CURRENT_RESULT (array) you must iterate manually each element of the list and append it to your new array variable, something like "for ELEMENT in $VAR_UTILS_YAML_CURRENT_RESULT; set --append YOUR_VARIABLE $ELEMENT; end" because "set YOUR_VARIABLE $VAR_UTILS_YAML_CURRENT_RESULT" will never work

function _yamlCheckConfigFile
    if not test -f "$argv[1]"
        haltExecution "File not found: $argv[1]"
    end
end

function _yamlResetGlobalVariables
    set -e -g YAML_VALUE
end

# Result of any given yq expression: https://mikefarah.gitbook.io/yq/how-it-works
function yamlGetValue
    _yamlCheckConfigFile "$argv[2]"
    _yamlResetGlobalVariables
    set -g YAML_VALUE (yq -M "$argv[1]" "$argv[2]") # Stored as global variable because we cannot return it
end
function mv
    rsync --archive -hh --partial --stats --progress --remove-source-files $argv
end
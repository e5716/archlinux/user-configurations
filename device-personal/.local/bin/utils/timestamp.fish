#!/bin/fish

function timestamp
    echo (date +'%d-%m-%y_%H-%M-%S')
end
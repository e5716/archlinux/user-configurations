function resetPermissions
    set location "$argv[1]"
    set ownerUser "$argv[2]"
    set ownerGroup "$argv[3]"

    # Optimal solution, it will find only those files/directories not owned by the user (much faster) and execute one instruction for all the files (batch mode). You can integrate many instructions with the separator "\;" like below
    # The dot "." is the pattern for finding files inside the directories
    fd -u -t d -o "!$ownerUser" -X chown "$ownerUser":"$ownerGroup" {} \; -X chmod 0700 {} \; . "$location"
    fd -u -t f -o "!$ownerUser" -X chown "$ownerUser":"$ownerGroup" {} \; -X chmod 0600 {} \; . "$location"
end
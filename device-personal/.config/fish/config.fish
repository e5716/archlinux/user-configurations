# Remove start message
set fish_greeting

# Only executed when there is a new login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR -eq 1
        exec Hyprland
    end
end

# NOT NEEDED ANYMORE. Refer to the "env.fish" file instead.
# Defining the root address where all my scripts are stored. This is needed as I'm using a symlink on the default ~/.config/fish/config.fish. Source: https://stackoverflow.com/a/19673602
# set --export FISH_CUSTOM_ROOT (dirname (readlink -m (status --current-filename)))

function cp
    rsync --info=stats1,progress2 --archive --inplace --no-whole-file -hh --partial $argv
end
#!/bin/fish

source $MY_SCRIPTS_ROOT/utils/standard-streams.fish
source $MY_SCRIPTS_ROOT/utils/yaml.fish
source $MY_SCRIPTS_ROOT/backup/cmd-superuser.fish
source $MY_SCRIPTS_ROOT/backup/cmd-7z.fish
source $MY_SCRIPTS_ROOT/backup/cmd-rsync.fish
source $MY_SCRIPTS_ROOT/backup/cmd-rm.fish
source $MY_SCRIPTS_ROOT/backup/cmd-borg.fish
source $MY_SCRIPTS_ROOT/backup/cmd-rclone.fish
source $MY_SCRIPTS_ROOT/backup/cmd-detox.fish
source $MY_SCRIPTS_ROOT/backup/mounted-disks.fish
source $MY_SCRIPTS_ROOT/backup/module-milf.fish
source $MY_SCRIPTS_ROOT/backup/module-android-sync.fish
source $MY_SCRIPTS_ROOT/backup/module-system.fish
source $MY_SCRIPTS_ROOT/backup/module-x2c.fish

function setScriptVariables
    set -g backupConfigFile "$MY_SCRIPTS_CONFIG_ROOT/personal-backup.yaml"
    set -g offlineDisksConfigFile "$MY_SCRIPTS_CONFIG_ROOT/offline-disks.yaml"
    set -g androidSyncConfigFile "$MY_SCRIPTS_CONFIG_ROOT/android-sync.yaml"
end
function init
    setScriptVariables
    superuserStorePassword
end

init
runMilf
runAndroidSync $argv
runSystemBackup
runExtrasToCloud
exit 0
#/bin/bash

TIMEZONE=""
CITY=""
EMOJI_FLAG=""

handleCity() {
    case "$1" in
        "BOG")
            TIMEZONE="America/Bogota"
            CITY="Bogotá"
            EMOJI_FLAG="🇨🇴"
            ;;
        "TIJ")
            TIMEZONE="America/Tijuana"
            CITY="Tijuana"
            EMOJI_FLAG="🇲🇽"
            ;;
        "ZUR")
            TIMEZONE="Europe/Zurich"
            CITY="Zürich"
            EMOJI_FLAG="🇨🇭"
            ;;
        "GBA")
            TIMEZONE="America/Buenos_Aires"
            CITY="Buenos Aires"
            EMOJI_FLAG="🇦🇷"
            ;;
        "local")
            TIMEZONE=$(timedatectl --value show -p Timezone) # It displays only the timezone
            CITY=$(echo $TIMEZONE | cut -d '/' -f 2)
            EMOJI_FLAG="⏰"
            ;;
    esac

    displayCityTime
}
displayCityTime() {
    TZ="$TIMEZONE" date +"$EMOJI_FLAG $CITY %R %Z"
}

while getopts 'c:' OPTION; do
    case "$OPTION" in
        c)
            handleCity "$OPTARG"
            ;;
        *)
            exit 1
            ;;
    esac
done
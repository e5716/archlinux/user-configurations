# My personal backups

My backup system solution. It tries to be KISS.

> **FRIENDLY REMINDER: MAGNETS ARE A SERIOUS THREAT TO YOUR DISKS**

This backup solution is trying to be compliant with 3-2-1 Backup-Regel:

- 3 copies of data
- 2 different locations
- 1 offsite

Note: this document is partially outdated, many things have changed.

## ToDo

- [ ] Run offline and cloud mirroring with GNU Parallel

---

# Requirements (ArchLinux)

- fish
- go-yq
- fd
- borg
- rsync
- rclone
- ~~tar, zst~~ 7z, because currently the only archiver app in F-Droid can read 7-Zip
- split
- detox
- adb
- adbsync

# Lessons learned

- Shell scripting is the most efficient way to solve a backup script. Python just adds complexity because most commands that need to be run are shell commands, not Python functions; Fish shell syntax is overall a good resource to solve this.

- I like Python testing framework, but I won't use it here. Most results cannot be easily tested.

- It looks like big files can be copied much faster than smaller ones; that's the reason I like to copy the whole Borg repo that inside contains a copy of the whole system image; both offline and cloud.

- **DON'T use USB hubs**, they introduce lots of bugs to all the ports they "can handle", including USB. At least in Linux.

# Usage

The magic starts at `$MY_SCRIPTS_ROOT/personal-backup.fish` and it depends from custom solutions available in this repo as well.

Its configuration file is located at `$MY_SCRIPTS_CONFIG_ROOT/personal-backup.yaml`

# Architecture

```mermaid
graph LR
graph LR
    A{Archlinux: 
        /, /home
    } --> B[
        Borg repo - disk
    ]
    B --> C(mirror1 - disk)
    B --> D(mirror2 - disk)
    B --> E(mirror3 - cloud)
    B --> F(mirror4 - cloud)
```
Mirrors can be either physical disks or cloud storage; all of them will hold a copy of the original Borg repository of the full image of the system

# Optional modules

1. MILF: Most Important Local Files - create a compressed image and distribute it anywhere you can: smartphones, cloud storage, pendrives USB, etc.
1. Android bidirectional sync
1. Extra files to cloud: sync anything you want to an Rclone-encrypted directory in cloud; use symlinks on the source folder
#!/bin/fish

# You can customize these folders
set myFilesDirectoriesList "$HOME/Documents" "$HOME/Downloads" "$HOME/Music" "$HOME/Pictures" "$HOME/Music" "$HOME/Videos"
set appLaunchersDirectoriesList "/usr/share/applications" "$HOME/.local/share/applications"

# Customizing the find command to search only what we need, as fzf needs find or other search tool for filtering results
#set fzfFinder "find $myFilesDirectoriesList $appLaunchersDirectoriesList -xtype f ! -path '*.git/*'" # With "-xtype" we include symlinks and "! -path" excludes git folders
set fzfFinder "fd --type f --type l --hidden --ignore-file $MY_SCRIPTS_CONFIG_ROOT/my-generic-launcher.ignore . $myFilesDirectoriesList $appLaunchersDirectoriesList"

set selectedFile (FZF_DEFAULT_COMMAND=$fzfFinder fzf) # fzf has an interesting keybinding that can be called as a flag "--bind 'enter:execute-silent:xdg-open {}'" for opening files, but it's not possible to detach the process https://man.archlinux.org/man/fzf.1.en#COMMAND_EXECUTION

# As this script should run from a window that executes something like "foot sh -c <this_script>" I never found the way to detach the process using either nohup or disown; impossible to solve it. I think this is related to the shell executed inside of that window. The solution appears to be "setsid"
if string length --quiet "$selectedFile"
    if string match --regex --quiet '\.desktop$' "$selectedFile"
        setsid --fork gio launch "$selectedFile" # For launching .desktop files
    else
        setsid --fork xdg-open "$selectedFile" # For opening files associated with their default program (MIME)
    end
else
    exit 1
end

exit 0
function sync-to-android
    fish $MY_SCRIPTS_ROOT/android-sync.fish --operation push --serial $argv[1] --name $argv[2]
end
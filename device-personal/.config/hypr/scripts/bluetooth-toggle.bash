#!/bin/bash

# Based on this script: https://www.jvt.me/posts/2020/03/27/toggle-bluetooth-connection/

toggleStatus() {
	poweredStatus=$(bluetoothctl show "$1" | grep 'Powered:' | cut -d ' ' -f 2)

	case "$poweredStatus" in
		('yes') bluetoothctl power off ;;
		('no') bluetoothctl power on ;;
	esac
}

defaultDevice=$(bluetoothctl list | grep 'Controller' | grep 'default' | cut -d ' ' -f 2)

if [ ! -z "$defaultDevice" -a "$defaultDevice" != " " ]; then
	toggleStatus $defaultDevice
fi
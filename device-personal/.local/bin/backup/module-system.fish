# The borg repo MUST be created and configured properly before running this subroutine. You only must do this once: https://borgbackup.readthedocs.io/en/stable/usage/init.html
# The symlinks will be stored as symlinks and ownership will be respected: https://borgbackup.readthedocs.io/en/stable/usage/general.html#support-for-file-metadata
# Borg env vars: https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables
# The sparse files are those files that virtually have a predefined size, but in reality they have a real size smaller. Borg does not treat specially these files when creating an archive, but you can use a special flag "--sparse" when "borg extract" your archive so these files are recreated just like the source. https://borgbackup.readthedocs.io/en/stable/faq.html?highlight=sparse#which-file-types-attributes-etc-are-not-preserved https://borgbackup.readthedocs.io/en/stable/usage/extract.html
# To init your repo, is suggested you choose a hardware-accelerated encryption. Take a look to https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-modes and choose wisely. Hint: take a look to "lscpu" and https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/tree/arch/x86/include/asm/cpufeatures.h for CPU capabilities
# At this moment and for some reason, Borg extract does not reflect the BSD flags, some of my files are IMMUTABLE but when extracting them they do not appear with that attribute

# Rclone configuration
# 1. Create an encrypted Rclone configuration file: https://rclone.org/docs/#configuration-encryption
# 2. Configure the remotes you want to connect to Rclone
# 3. Create an encrypted layer for each remote you want to mirror files (acts like a wrapper of a remote): https://rclone.org/crypt/
# 3.1 Encrypt filenames
# 3.2 Encrypt directory names
# 3.3 Set a custom encryption passphrase
# 3.3 Set a custom salt passphrase

function _resetOwnershipGeneric
    superuserExec "source $MY_SCRIPTS_ROOT/utils/file-permissions.fish; resetPermissions '$argv[1]' $argv[2] $argv[3]"; or haltExecution
end
function _dumpPacmanPackages
    pacman -Qie > "$argv[1]"
    pacman -Qim > "$argv[2]"
    pacman -Qi > "$argv[3]"
end
function _checkIsMountedBackupDisk
    yamlGetValue ".archlinux_backup.destination_offline_disk" "$backupConfigFile"
    checkIsDiskMounted "$YAML_VALUE"
end
function _resetBorgConfigOwnership
    _resetOwnershipGeneric "$argv[1]" "$argv[2]" "$argv[3]"
end
function _deleteBorgCache
    superuserExec "/bin/rm -rf $argv[1]"
end
function _runBorgWorkaround
    # Run workaround or further executions of borg would fail
    _deleteBorgCache "$HOME/.cache/borg"
    _resetOwnershipGeneric "$HOME/.config/borg" "$argv[1]" "$argv[2]"
end
function _borgCheckRepo
    yamlGetValue ".archlinux_backup.borg_repository.run_check" "$backupConfigFile"
    if test "$YAML_VALUE" = "true"
        borgCheck "$argv[1]" "$argv[2]"; or haltExecution
    end
end
function _borgCreateNewArchive
    yamlGetValue ".archlinux_backup.borg_repository.source_path" "$backupConfigFile"
    set borgSourcePath "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.borg_repository.archive_name" "$backupConfigFile"
    set borgArchiveName "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.borg_repository.ignore_file" "$backupConfigFile"
    set borgIgnoreFile "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.borg_repository.compression" "$backupConfigFile"
    set borgCompression "$YAML_VALUE"

    superuserExec "source $MY_SCRIPTS_ROOT/backup/cmd-borg.fish; borgCreateArchive '$argv[1]' '$argv[2]' '$borgCompression' '$MY_SCRIPTS_CONFIG_ROOT/$borgIgnoreFile' '$borgArchiveName' '$borgSourcePath'"; or haltExecution
end
function _resetOwnershipOfBackupFiles
    _resetOwnershipGeneric "$argv[1]" "$argv[2]" "$argv[3]"
end
function _borgPruneOldArchives
    yamlGetValue ".archlinux_backup.borg_repository.keep_last_n_files" "$backupConfigFile"
    borgPrune "$argv[1]" "$argv[2]" "$YAML_VALUE"
end
function _cleanPacmanDumps
    rmFileOrDir "$argv[1]" "$argv[2]" "$argv[3]"
end
function _offlineMirroring
    yamlGetValue ".archlinux_backup.mirroring.offline_storage[]" "$backupConfigFile"
    for DESTINATION in $YAML_VALUE
        checkIsDiskMounted "$DESTINATION"

        yamlGetValue ".$DESTINATION.mountpoint" "$offlineDisksConfigFile"
        rsyncCopy "$argv[1]" "$YAML_VALUE"
    end
end
function _cloudMirroring
    yamlGetValue ".common.rclone_config_pwd" "$backupConfigFile"
    set rclonePwd "$YAML_VALUE"

    yamlGetValue ".archlinux_backup.mirroring.cloud_storage[]" "$backupConfigFile"
    for DESTINATION in $YAML_VALUE
        rcloneSync "$rclonePwd" "$argv[1]" "$DESTINATION"
    end
end

function runSystemBackup
    # Used in more than one instruction
    yamlGetValue ".archlinux_backup.pacman_dump_packages.explicit" "$backupConfigFile"
    set pacmanDumpFileExplicit "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.pacman_dump_packages.aur" "$backupConfigFile"
    set pacmanDumpFileAur "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.pacman_dump_packages.all" "$backupConfigFile"
    set pacmanDumpFileAll "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.files_ownership.user" "$backupConfigFile"
    set ownerUser "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.files_ownership.group" "$backupConfigFile"
    set ownerGroup "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.borg_repository.repo_path" "$backupConfigFile"
    set borgRepoPath "$YAML_VALUE"
    yamlGetValue ".archlinux_backup.borg_repository.repo_pwd" "$backupConfigFile"
    set borgPwd "$YAML_VALUE"

    # Execute the system backup
    _checkIsMountedBackupDisk
    _dumpPacmanPackages "$pacmanDumpFileExplicit" "$pacmanDumpFileAur" "$pacmanDumpFileAll"

    _runBorgWorkaround "$ownerUser" "$ownerGroup" 
    _borgCheckRepo "$borgRepoPath" "$borgPwd"
    _runBorgWorkaround "$ownerUser" "$ownerGroup" 
    _borgCreateNewArchive "$borgRepoPath" "$borgPwd"
    _resetOwnershipOfBackupFiles "$borgRepoPath" "$ownerUser" "$ownerGroup"
    _runBorgWorkaround "$ownerUser" "$ownerGroup" 
    _borgPruneOldArchives "$borgRepoPath" "$borgPwd"
    _cleanPacmanDumps "$pacmanDumpFileExplicit" "$pacmanDumpFileAur" "$pacmanDumpFileAll"

    # Mirror the system backup to external destinations
    _offlineMirroring "$borgRepoPath"
    _cloudMirroring "$borgRepoPath"
end
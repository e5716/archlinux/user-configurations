function rcloneSync
    # The "--update" flag helps to overwrite existing files that are newer at the source. Without this flag, some files are never updated on the destination
    # We use "--delete-during" instead of "--delete-after" because Fastmail is a pain in the ass and its storage limits are easily reached
    # rclone only copies files inside specified dir; that's the reason I use symlinks in "$HOME/.personal-backup/send-to-*" with all the source folders so those can be created as well and the desired folder structure can be respected
    RCLONE_CONFIG_PASS="$argv[1]" rclone --delete-during --update --copy-links --progress --verbose sync --create-empty-src-dirs "$argv[2]" "$argv[3]"
    return $0
end
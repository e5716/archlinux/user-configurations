#!/bin/fish

# Paths must be always absolute because if the alias is ran from another CWD it won't find the sources
source $MY_SCRIPTS_ROOT/utils/standard-streams.fish
source $MY_SCRIPTS_ROOT/utils/yaml.fish

function defineListLimit
    yamlGetValue ". | length" "$configurationFile"
    set -g upperLimitOfConfigList (math $YAML_VALUE - 1)
end

function associateMimetypesPerApplication
    yamlGetValue ".[$argv[1]].application" "$configurationFile"
    set appName $YAML_VALUE

    yamlGetValue ".[$argv[1]].mimetypes[]" "$configurationFile"

    for mimetype in $YAML_VALUE
        xdg-mime default "$appName.desktop" "$mimetype"
    end
end

set -g configurationFile "$MY_SCRIPTS_CONFIG_ROOT/file-associations.yaml"
defineListLimit
for I in (seq 0 $upperLimitOfConfigList)
    associateMimetypesPerApplication "$I"
end

exit 0
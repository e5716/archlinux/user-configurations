# Archlinux: my configurations (dotfiles) and scripts

A repo for tracking relevant configuration files for my Arch user. It includes my personal scripts for various tasks, like a complete and automated backup solution.

# Requirements

- GNU Stow
- [go-yq](https://archlinux.org/packages/extra/x86_64/go-yq/)

# Instructions

For deployment of config files simply run `stow` twice

1. `stow --dir=<path_to_this_repo_folder> --target=$HOME common` for deploying common settings to all profiles
1. `stow --dir=<path_to_this_repo_folder> --target=$HOME <PREFERRED_PACKAGE>` for deploying the profile you want to install

## Valid PREFERRED_PACKAGE names

- `device-personal`
- `device-work`

# About specific configurations

## Fish

The Fish Shell configurations found in this repo now uses the standard way to load files by using their `conf.d` and `functions` folders.

According to Fish docs, [source files are loaded this way](https://fishshell.com/docs/current/language.html#configuration-files):

1. Sources at `~/.config/fish/conf.d` in natural order, so the file called `10-something.fish` would be executed before `99-something.fish`
2. Sources at other directories not used by this solution
3. The last file to be executed is `~/.config/fish/config.fish`

Also, my Fish functions are stored directly at the standard `~/.config/fish/functions` folder as a best practice, so we can take some advantage of autoloading functions. [Read more](https://fishshell.com/docs/current/language.html#autoloading-functions).

At the end of the day, the `config.fish` file should be small and it would not be responsible of loading more source files.

## My scripts

See `docs` folder.

## VSCodium

I use VSCodium as replacement of VSCode for privacy rea$on$. In these texts I will refer to them indifferently.

VSCode doesn't offer a friendly solution to use different profiles compatible with this repository (that uses GNU Stow).

The alternative is to have a global `settings.json` and `keybindings.json` that will be common to all our devices. Then, we'll use a VSCode feature called ["Workspaces - Multiroot workspaces"](https://code.visualstudio.com/docs/editor/workspaces#_multiroot-workspaces) that allow specific configurations to be applied to a subset of folders defined on the same workspace file.

You can load the workspace file directly in VSCode and it will read its own settings. Also, some workspaces provide extensions to be installed in VSCode with a single click. You can enable extensions manually for only that workspace directly on the UI.

You must store workspaces at `$HOME/.config/VSCodium/User/my-workspaces`; otherwise when storing them at `Workspaces` folder they're deleted by VSCodium automatically.

### Relevant CLI commands for handling extensions

- `codium --list-extensions` returns a list of installed extensions
- `codium --install-extension <EXTENSION_ID>` installs an extension from its ID

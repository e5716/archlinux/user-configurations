function checkIsDiskMounted
    yamlGetValue ".$argv[1].disk_uuid" "$offlineDisksConfigFile"
    set diskUUID "$YAML_VALUE"

    set CONST_PATH_PROC_MOUNTS "/dev/mapper/luks-"
    set fullPathOfDisk "$CONST_PATH_PROC_MOUNTS$diskUUID"
    cat /proc/mounts | grep "$fullPathOfDisk" > /dev/null; or haltExecution "Disk '$fullPathOfDisk' is not mounted"
end
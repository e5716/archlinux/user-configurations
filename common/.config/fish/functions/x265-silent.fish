function x265-silent
	ffmpeg -i "$argv" -c:v libx265 -preset medium -an "$argv.mkv"
end
function rmFileOrDir
    /usr/bin/rm -rf $argv # Because I don't want to execute the alias "rm" that should point to the recycle bin manager
    return $0
end